import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import { Login } from './components/Login/Login';
import { Registration } from './components/Registration/Registration';
import { Header } from './components/Header/Header';
import { store } from './store/rootReducer';
import { PrivateRoute } from './components/PrivateRoute/PrivateRoute';

const root = ReactDOM.createRoot(
	document.getElementById('root') as HTMLElement
);

const login = (
	<>
		<Header />
		<Login />
	</>
);

const register = (
	<>
		<Header />
		<Registration />
	</>
);
root.render(
	<Provider store={store}>
		<React.StrictMode>
			<BrowserRouter>
				<Routes>
					<Route
						path='/'
						element={
							localStorage.getItem('authToken') ? (
								<Navigate to='/courses' replace />
							) : (
								<Navigate to='/login' replace />
							)
						}
					/>
					<Route path='/courses' element={<App />} />
					<Route
						path='/courses/add'
						element={
							<PrivateRoute>
								<App type='course_add' />
							</PrivateRoute>
						}
					/>
					<Route
						path='/course/:courseId'
						element={<App type='course_info' />}
					/>
					<Route
						path='/courses/update/:courseId'
						element={
							<PrivateRoute>
								<App type='course_update' />
							</PrivateRoute>
						}
					/>
					<Route path='/login' element={login} />
					<Route path='/registration' element={register} />
				</Routes>
			</BrowserRouter>
		</React.StrictMode>
	</Provider>
);
