import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { CourseInfo } from './components/CourseInfo/CourseInfo';
import { Courses } from './components/Courses/Courses';
import { Header } from './components/Header/Header';
import './base.css';
import './App.css';
import { redirectDocument, useParams } from 'react-router-dom';
import { CourseForm } from './components/CourseForm/CourseForm';
import { AppState, useAppDispatch } from './store/rootReducer';
import { fetchCurrentUser } from './store/user/thunk';
import { fetchCourses } from 'src/store/courses/thunk';
import { fetchAuthors } from 'src/store/authors/thunk';

interface AppProps {
	type?: string;
}

const App: React.FC<AppProps> = (props) => {
	const dispatch = useAppDispatch();
	const user = useSelector((state: AppState) => state.user);
	const courseId = useParams().courseId;
	const coursesList = useSelector((store: AppState) => store.courses);
	const authorsList = useSelector((store: AppState) =>
		store.authors.map((a) => a.id)
	);

	useEffect(() => {
		if (coursesList.length < 1) dispatch(fetchCourses);
		if (authorsList.length < 1) dispatch(fetchAuthors);
		dispatch(fetchCurrentUser(localStorage.getItem('authToken')));
	}, []);
	const header = (
		<Header
			type={user.isAuth ? 'logged-in' : 'not-logged-in'}
			username={user.isAuth ? user.name : ''}
		/>
	);
	return (
		<div className='app'>
			{header}
			{props.type === 'course_info' ? (
				<CourseInfo id={courseId} onBack={() => redirectDocument('/courses')} />
			) : props.type === 'course_add' ? (
				<CourseForm />
			) : props.type === 'course_update' ? (
				<CourseForm courseId={courseId} />
			) : (
				<Courses className='courses' />
			)}
		</div>
	);
};

export default App;
