import { Course } from './store/courses/types';
import { Author } from './store/authors/types';
import { apiAddress } from './constants';

export async function fetchCourses(): Promise<Course[]> {
	const response: { successful: boolean; result?: Course[] } = await fetch(
		`${apiAddress}/courses/all`
	).then((r) => r.json());
	return response.successful ? (response.result as Course[]) : ([] as Course[]);
}

export async function fetchCourse(id: string): Promise<Course> {
	const response: { successful: boolean; result?: Course } = await fetch(
		`${apiAddress}/courses/${id}`
	).then((r) => r.json());
	return response.successful ? (response.result as Course) : ({} as Course);
}

export async function fetchAuthors(): Promise<Author[]> {
	const response: { successful: boolean; result?: Author[] } = await fetch(
		`${apiAddress}/authors/all`
	).then((r) => r.json());
	return response.successful ? (response.result as Author[]) : ([] as Author[]);
}

export async function checkAuth(): Promise<{
	name: string;
	email: string;
	isAuth: boolean;
	token: string;
	role: string;
}> {
	const defaultResponse = {
		name: '',
		email: '',
		isAuth: false,
		token: '',
		role: '',
	};
	if (!localStorage.getItem('authToken')) return defaultResponse;
	const response = await fetch(`${apiAddress}/users/me`, {
		method: 'GET',
		headers: { Authorization: localStorage.getItem('authToken') },
	}).then((r) => r.json());
	if (!response.successful) localStorage.removeItem('authToken');
	return response.successful
		? {
			name: response.result.name,
			email: response.result.email,
			isAuth: true,
			token: localStorage.getItem('authToken'),
			role: response.result.role,
		}
		: defaultResponse;
}

export async function loginUser(
	email: string,
	password: string,
	redirect: (url: string) => void
) {
	const response = await fetch(`${apiAddress}/login`, {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({ email, password }),
	}).then((r) => r.json());

	if (!response.successful) throw new Error(JSON.stringify(response));
	else {
		localStorage.setItem('authToken', response.result);
		redirect('/courses');
	}
}

export async function logoutUser() {
	if (!localStorage.getItem('authToken')) throw Error('user not authorized');
	const response = await fetch(`${apiAddress}/logout`, {
		method: 'DELETE',
		headers: { Authorization: localStorage.getItem('authToken') },
	});
	return response.toString();
}
