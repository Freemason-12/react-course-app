import React from 'react';
import { Button } from 'src/common/Button/Button';
import { Input } from 'src/common/Input/Input';
import 'src/base.css';
import styles from './SearchBar.module.css';
interface SearchBarType {
	onSearch?: (e: Event) => void;
}
const SearchBar: React.FC<SearchBarType> = (props) => {
	const clicked = (e) => {
		e.preventDefault();
		props.onSearch(e);
	};
	return (
		<form className={styles.searchbar}>
			<Input
				labelText=''
				placeHolder='Input text'
				inputType='text'
				onChange={() => {}}
				required={false}
			/>
			<Button onClick={clicked}>Search</Button>
		</form>
	);
};

export { SearchBar };
