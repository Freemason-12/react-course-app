interface CourseCardType {
	id: string;
	name: string;
	description: string;
	duration: number;
	date: Date;
	authors: string[];
}

export { CourseCardType };
