import React from 'react';
import { CourseCardType } from './CourseCard.types';
import { Button } from 'src/common/Button/Button';
import { AppState, useAppDispatch } from 'src/store/rootReducer';
import 'src/base.css';
import styles from './CourseCard.module.css';
import icon_trash from './img/Icon-Trash.svg';
import icon_edit from './img/Icon-Edit.svg';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
// import { removeCourseAction } from 'src/store/courses/actions';
import { removeCourse } from 'src/store/courses/thunk';

const CourseCard: React.FC<CourseCardType> = (props) => {
	const isAdmin = useSelector((store: AppState) => store.user.role === 'admin');
	const hours = Math.floor(props.duration / 60);
	const minutes = props.duration % 60;
	const dispatch = useAppDispatch();
	return (
		<div className={styles.course_card}>
			<h2 className={styles.course_name}>{props.name}</h2>
			<div className={styles.course_container}>
				<p className={styles.course_content}>{props.description}</p>
				<div className={styles.course_meta}>
					<div className={styles.authors}>
						<b>Authors: </b>
						{props.authors.join(', ')}
					</div>
					<div className={styles.duration}>
						<b>Duration: </b>
						{hours ? `${hours} hours ` : ' '}
						{minutes ? `${minutes} minutes` : ''}
					</div>
					<div className={styles.date}>
						<b>Created: </b>
						{props.date.toLocaleDateString('en-US', {
							day: 'numeric',
							month: 'long',
							year: 'numeric',
						})}
					</div>
					<div className={styles.course_buttons}>
						<Link to={`/course/${props.id}`}>
							<Button>Show Course</Button>
						</Link>
						{isAdmin ? (
							<>
								<Button
									className={styles.iconbutton}
									onClick={() =>
										dispatch(
											removeCourse(props.id, localStorage.getItem('authToken'))
										)
									}
								>
									<img src={icon_trash} />
								</Button>
								<Link to={`/courses/update/${props.id}`}>
									<Button className={styles.iconbutton}>
										<img src={icon_edit} />
									</Button>
								</Link>
							</>
						) : (
							<></>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

export { CourseCard };
