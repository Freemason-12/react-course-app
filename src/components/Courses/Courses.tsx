import React from 'react';
import { SearchBar } from './components/SearchBar/SearchBar';
import { EmptyCourseList } from '../EmptyCourseList/EmptyCourseList';
import { AppState } from 'src/store/rootReducer';
import { useSelector } from 'react-redux';
import { CourseCard } from './components/CourseCard/CourseCard';
import { CoursesType } from './Courses.types';
import { Button } from 'src/common/Button/Button';
import { Link } from 'react-router-dom';
import styles from './Courses.module.css';

const getAuthorsNames = (authorsIds: string[], authors: Map<string, string>) =>
	authorsIds.map((x) => authors.get(x));

const Courses: React.FC<CoursesType> = (props) => {
	const isAdmin = useSelector((store: AppState) => store.user.role === 'admin');
	const coursesList = useSelector((store: AppState) => store.courses);
	const authorsList = useSelector((store: AppState) =>
		store.authors.map((a) => a.id)
	);

	const authorsMap = useSelector((store: AppState) => {
		const map = new Map();
		store.authors.map((author) => map.set(author.id, author.name));
		return map;
	});

	if (!coursesList.length) return <EmptyCourseList />;
	return (
		<div className={props.className}>
			<div className={styles.top_bar}>
				<SearchBar onSearch={() => {}} />
				{isAdmin ? (
					<Link className={styles.add_new_course} to='/courses/add'>
						<Button>Add New Course</Button>
					</Link>
				) : (
					<></>
				)}
			</div>
			{coursesList.map((course) => {
				return (
					<CourseCard
						key={course.id}
						id={course.id}
						name={course.title}
						description={course.description}
						duration={course.duration}
						date={new Date(course.creationDate)}
						authors={getAuthorsNames(authorsList, authorsMap)}
					/>
				);
			})}
		</div>
	);
};

export { Courses };
