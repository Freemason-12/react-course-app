import React, { useState } from 'react';
import { Input } from 'src/common/Input/Input';
import { Button } from 'src/common/Button/Button';
import { Link, useNavigate } from 'react-router-dom';
import { apiAddress } from 'src/constants';
import 'src/base.css';
import styles from './Registration.module.css';

async function registerRequest(
	name: string,
	email: string,
	password: string,
	redirect: (url: string) => void
) {
	const response = await fetch(`${apiAddress}/register`, {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({ name, email, password }),
	}).then((r) => r.json());
	if (!response.successful) throw Error(JSON.stringify(response));
	else redirect('/login');
}

function Registration() {
	const [name, setName] = useState(' ');
	const [email, setEmail] = useState(' ');
	const [password, setPassword] = useState(' ');
	const navigate = useNavigate();
	return (
		<div className={styles.container}>
			<h1 className={styles.header}>Registration</h1>
			<form
				onSubmit={(e) => {
					e.preventDefault();
					registerRequest(name, email, password, navigate);
				}}
				className={styles.form}
			>
				<Input
					labelText='Name'
					placeHolder='Input text'
					inputType='text'
					onChange={(e) => {
						setName(e.target.value);
					}}
					required={name === ''}
				/>
				<Input
					labelText='Email'
					placeHolder='Input text'
					inputType='text'
					onChange={(e) => {
						setEmail(e.target.value);
					}}
					required={email === ''}
				/>
				<Input
					labelText='Password'
					placeHolder='Input text'
					inputType='password'
					onChange={(e) => {
						setPassword(e.target.value);
					}}
					required={password === ''}
				/>
				<Button>Login</Button>
				<p>
					If you have an account you may{' '}
					<Link to='/login'>
						<b>Login</b>
					</Link>
				</p>
			</form>
		</div>
	);
}

export { Registration };
