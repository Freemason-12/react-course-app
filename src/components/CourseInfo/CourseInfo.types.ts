interface CourseInfoType {
	id: string;
	onBack: () => void;
	className?: string;
}

export { CourseInfoType };
