import React, { useEffect } from 'react';
import { CourseInfoType } from './CourseInfo.types';
import { Button } from 'src/common/Button/Button';
import { Link } from 'react-router-dom';
import { AppState } from 'src/store/rootReducer';
import 'src/base.css';
import styles from './CourseInfo.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCourse } from 'src/services';
import { addCourseAction } from 'src/store/courses/actions';

const CourseInfo: React.FC<CourseInfoType> = (props) => {
	const course = useSelector((store: AppState) =>
		store.courses.find((c) => c.id === props.id)
	);
	const dispatch = useDispatch();
	useEffect(() => {
		if (!course)
			fetchCourse(props.id).then((r) => dispatch(addCourseAction(r)));
	});
	const authors = useSelector((store: AppState) => {
		const map = new Map();
		store.authors.forEach((author) => map.set(author.id, author.name));
		return !course ? [] : course.authors.map((courseId) => map.get(courseId));
	});
	const duration = !course ? 0 : course.duration;
	const hours = Math.floor(duration / 60);
	const minutes = duration % 60;
	return !course ? (
		<></>
	) : (
		<div className={props.className}>
			<h1 className={styles.name}>{course.title}</h1>
			<div className={styles.internal}>
				<h2 className={styles.description_header}>Description: </h2>
				<div className={styles.data}>
					<div className={styles.description}>
						<p>{course.description}</p>
					</div>
					<table className={styles.course_meta}>
						<tbody>
							<tr>
								<td>
									<b>ID:</b>
								</td>
								<td>{course.id}</td>
							</tr>
							<tr>
								<td>
									<b>Duration:</b>
								</td>
								<td>
									{hours ? `${hours} hours` : ''}{' '}
									{minutes ? `${minutes} minutes` : ''}
								</td>
							</tr>
							<tr>
								<td>
									<b>Created:</b>
								</td>
								<td>
									{new Date(course.creationDate).toLocaleDateString('en-US', {
										day: 'numeric',
										month: 'long',
										year: 'numeric',
									})}
								</td>
							</tr>
							<tr>
								<td>
									<b>Authors:</b>
								</td>
								<td>{authors.join(', ')}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div className={styles.bottom_button}>
				<Link to='/courses'>
					<Button>Back</Button>
				</Link>
			</div>
		</div>
	);
};
export { CourseInfo };
