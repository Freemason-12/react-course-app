interface HeaderProps {
	type?: 'logged-in' | 'not-logged-in';
	username?: string;
}

export { HeaderProps };
