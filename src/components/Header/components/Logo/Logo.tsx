import React from 'react';
import { LogoProp } from './Logo.types.ts';
const Logo: React.FC<LogoProp> = (props) => {
	return <img src={props.src} alt='logo' />;
};

export { Logo };
