import React from 'react';
import { Logo } from './components/Logo/Logo.tsx';
import { Button } from 'src/common/Button/Button';
import { HeaderProps } from './Header.types.ts';
import { Link, useNavigate } from 'react-router-dom';
// import { useDispatch } from 'react-redux';
import { useAppDispatch } from 'src/store/rootReducer';
import 'src/base.css';
import logo from './logo.png';
import styles from './Header.module.css';
import { removeCurrentUser } from 'src/store/user/thunk.ts';

const Header: React.FC<HeaderProps> = (props) => {
	let loginButton;
	const navigate = useNavigate();
	const dispatch = useAppDispatch();
	switch (props.type) {
		case 'not-logged-in':
			loginButton = (
				<Link to='/login'>
					<Button>Login</Button>
				</Link>
			);
			break;
		case 'logged-in':
			loginButton = (
				<>
					<label className={styles.profile_name}>{props.username}</label>
					<Button
						onClick={() => {
							dispatch(removeCurrentUser(localStorage.getItem('authToken')));
							localStorage.removeItem('authToken');
							navigate('/login');
						}}
					>
						Logout
					</Button>
				</>
			);
			break;
		default:
			loginButton = <></>;
			break;
	}
	return (
		<div className={styles.header}>
			<Logo src={logo} />
			<div>{loginButton}</div>
		</div>
	);
};
export { Header };
