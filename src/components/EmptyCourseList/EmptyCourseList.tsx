import React from 'react';
import { Button } from 'src/common/Button/Button';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { AppState } from 'src/store/rootReducer';
import 'src/base.css';
import styles from './EmptyCourseList.module.css';

function EmptyCourseList() {
	const isAdmin = useSelector((store: AppState) => store.user.role === 'admin');
	const navigate = useNavigate();
	return (
		<div className={styles.empty_course_list}>
			<div>
				<h2>Your List is Empty</h2>
				<p>Please use ’Add New Course’ button to add your first course</p>
				<Button
					onClick={() => {
						if (isAdmin) navigate('/courses/add');
						else
							alert(
								"You don't have permissions to create a course. Please log in as ADMIN"
							);
					}}
				>
					Add New Course
				</Button>
			</div>
		</div>
	);
}

export { EmptyCourseList };
