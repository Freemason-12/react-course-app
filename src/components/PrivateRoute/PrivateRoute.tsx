import React from 'react';
import { Navigate } from 'react-router-dom';
import { store } from 'src/store/rootReducer';

export function PrivateRoute({ children }) {
	const allowed = store.getState().user.role === 'admin';
	return allowed ? children : <Navigate to='/courses' />;
}
