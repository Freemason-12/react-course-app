import React, { useState, useReducer } from 'react';
import { useSelector } from 'react-redux';
import { store, useAppDispatch, AppState } from 'src/store/rootReducer';
import { Input } from 'src/common/Input/Input';
import { Button } from 'src/common/Button/Button';
import { Link, useNavigate } from 'react-router-dom';
import 'src/base.css';
import styles from './CourseForm.module.css';
import { AuthorItem } from './components/AuthorItem/AuthorItem';
import { Author } from 'src/store/authors/types';
import { Course } from 'src/store/courses/types';
import { addCourse, saveCourse } from 'src/store/courses/thunk';
import { addAuthor } from 'src/store/authors/thunk';

type Action = { type: string; author: Author };
function courseAuthorDispatcher(authors: Author[], action: Action) {
	switch (action.type) {
		case 'add':
			return [...authors, action.author];
		case 'remove':
			return authors.filter((x) => x.id !== action.author.id);
	}
}
const CourseForm: React.FC<{ courseId?: string }> = ({ courseId }) => {
	const course: Course = store
		.getState()
		.courses.find((c) => c.id === courseId);
	const [title, setTitle] = useState(course ? course.title : '');
	const [description, setDescription] = useState(
		course ? course.description : ''
	);

	const formatDuration = (filteredValue: number) => {
		const format = (num: number) =>
			num.toLocaleString('en-US', {
				minimumIntegerDigits: 2,
				useGrouping: false,
			});
		return `${format(Math.floor(filteredValue / 60))}:${format(filteredValue % 60)}`;
	};

	const [duration, setDuration] = useState(course ? course.duration : 0);
	const [formattedDuration, setFormattedDuration] = useState(
		course ? formatDuration(course.duration) : '00:00'
	);

	const updateDuration = (e) => {
		setDuration(Number(e.target.value));
		const filteredValue = Number(e.target.value.replaceAll(/[^0-9]/g, ''));
		e.target.value = filteredValue === 0 ? '' : filteredValue;
		setFormattedDuration(formatDuration(filteredValue));
	};

	const authors = useSelector((store: AppState) => store.authors);
	const dispatchStore = useAppDispatch();

	const [authorsList, authorsListDispatch] = useReducer(
		courseAuthorDispatcher,
		authors
	);
	const [courseAuthors, courseAuthorsDispatch] = useReducer(
		courseAuthorDispatcher,
		course
			? store.getState().authors.filter((a) => course.authors.includes(a.id))
			: []
	);

	const [newAuthorName, setNewAuthorName] = useState('');
	const addNewAuthor = () => {
		authorsListDispatch({
			type: 'add',
			author: { id: `${Date.now()}`, name: newAuthorName },
		});
	};

	const navigate = useNavigate();
	const createNewCourse = () => {
		const fine =
			title.trim() !== '' &&
			description.trim() !== '' &&
			formattedDuration !== '00:00' &&
			courseAuthors.length > 0;
		if (fine) {
			const fd = new Date(Date.now());
			const finalDateString = `${fd.getMonth() + 1}/${fd.getDate()}/${fd.getFullYear()}`;
			const finalCourse: Course = {
				id: course ? course.id : `${Date.now()}`,
				title,
				description,
				creationDate: course ? course.creationDate : finalDateString,
				duration,
				authors: courseAuthors.map((author) => author.id),
			};
			courseAuthors.forEach((author) => {
				if (!authors.find((a) => a.id === author.id))
					dispatchStore(addAuthor(author, localStorage.getItem('authToken')));
			});
			const finalDispatch = course ? saveCourse : addCourse;
			console.log(finalDispatch, course);
			dispatchStore(
				finalDispatch(finalCourse, localStorage.getItem('authToken'))
			);
			navigate('/courses');
		} else alert('not fine');
	};
	return (
		<div className={styles.container}>
			<h1 className={styles.name}>Course Edit/Create Page</h1>
			<div className={styles.internal}>
				<h2>Main Info</h2>
				<Input
					inputType='text'
					labelText='Title'
					placeHolder='Input text'
					onChange={(e) => setTitle(e.target.value)}
					required={title === ''}
					value={title}
				/>
				<Input
					inputType='textarea'
					labelText='Description'
					placeHolder='Input text'
					onChange={(e) => setDescription(e.target.value)}
					required={description === ''}
					value={description}
				/>
				<h2>Duration</h2>
				<div className={styles.duration}>
					<Input
						inputType='text'
						labelText='Duration'
						placeHolder='Input text'
						onChange={updateDuration}
						required={formattedDuration === '00:00'}
						value={duration.toString()}
					/>
					<div>
						<b>{formattedDuration}</b> hours
					</div>
				</div>
				<div className={styles.authors}>
					<div>
						<h2>Authors</h2>
						<div className={styles.authors_input_container}>
							<Input
								inputType='text'
								labelText='Author Name'
								placeHolder='Input text'
								required={false}
								onChange={(e) => setNewAuthorName(e.target.value)}
							/>
							<Button onClick={addNewAuthor}>Create Author</Button>
						</div>
						<div className={styles.authors_list}>
							<h3>Authors List</h3>
							{authorsList.map((author) => (
								<AuthorItem
									add={true}
									key={author.id}
									name={author.name}
									onAdd={() => {
										authorsListDispatch({ type: 'remove', author });
										courseAuthorsDispatch({ type: 'add', author });
									}}
									onRemove={() => {
										authorsListDispatch({ type: 'remove', author });
									}}
								/>
							))}
						</div>
					</div>
					<div>
						<h2>Course Authors</h2>
						{courseAuthors.length ? (
							courseAuthors.map((author) => (
								<AuthorItem
									key={author.id}
									name={author.name}
									onRemove={() => {
										authorsListDispatch({ type: 'add', author });
										courseAuthorsDispatch({ type: 'remove', author });
									}}
								/>
							))
						) : (
							<p>Author list is empty</p>
						)}
					</div>
				</div>
			</div>
			<div className={styles.bottom_button}>
				<Link to='/courses'>
					<Button>Cancel</Button>
				</Link>
				<Button onClick={createNewCourse}>
					{course ? 'Update Course' : 'Create Course'}
				</Button>
			</div>
		</div>
	);
};

export { CourseForm };
