import React from 'react';
import trash from './trash.svg';
import add from './add.svg';
import styles from './AuthorItem.module.css';

interface AuthorItemProps {
	name: string;
	add?: boolean;
	onAdd?: () => void;
	onRemove?: () => void;
}

const AuthorItem: React.FC<AuthorItemProps> = (props) => {
	const typeButton = props.add ? (
		<button className={styles.author_item_button} onClick={props.onAdd}>
			<img src={add} alt='remove' />
		</button>
	) : (
		''
	);
	return (
		<div className={styles.author_item}>
			<p>{props.name}</p>
			{typeButton}
			<button className={styles.author_item_button} onClick={props.onRemove}>
				<img src={trash} alt='remove' />
			</button>
		</div>
	);
};

export { AuthorItem };
