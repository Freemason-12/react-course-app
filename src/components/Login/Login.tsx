import React, { useState } from 'react';
import { Input } from 'src/common/Input/Input';
import { Button } from 'src/common/Button/Button';
import { Link, useNavigate } from 'react-router-dom';
import 'src/base.css';
import styles from './Login.module.css';
import { loginUser } from 'src/services';

function Login() {
	const navigate = useNavigate();

	const [email, setEmail] = useState(' ');
	const [password, setPassword] = useState(' ');
	return (
		<div className={styles.container}>
			<h1 className={styles.header}>Login</h1>
			<form
				className={styles.form}
				onSubmit={(e) => {
					e.preventDefault();
					loginUser(email, password, navigate);
				}}
			>
				<Input
					labelText='Email'
					placeHolder='Input Text'
					onChange={(e) => setEmail(e.target.value)}
					required={email === ''}
					inputType='text'
				/>
				<Input
					labelText='Password'
					placeHolder='Input Text'
					onChange={(e) => setPassword(e.target.value)}
					required={password === ''}
					inputType='password'
				/>
				<Button>Login</Button>
				<p>
					If you don't have an account you may{' '}
					<Link to='/registration'>
						<b>Registration</b>
					</Link>
				</p>
			</form>
		</div>
	);
}

export { Login };
