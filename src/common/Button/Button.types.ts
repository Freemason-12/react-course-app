interface ButtonProp {
	children: React.ReactNode;
	onClick?: (e) => void;
	className?: string;
}

export { ButtonProp };
