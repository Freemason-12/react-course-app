import React from 'react';
import { ButtonProp } from './Button.types';
import 'src/base.css';
import styles from './Button.module.css';

const Button: React.FC<ButtonProp> = (props) => {
	return (
		<button
			className={`${styles.button} ${props.className}`}
			onClick={props.onClick}
		>
			{props.children}
		</button>
	);
};

export { Button };
