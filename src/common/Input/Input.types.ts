interface InputProps {
	inputType: string;
	labelText: string;
	placeHolder: string;
	required: boolean;
	onChange?: (e) => void;
	value?: string;
}

export default InputProps;
