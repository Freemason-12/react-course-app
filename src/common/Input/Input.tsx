import React, { useId, useState } from 'react';
import 'src/base.css';
import styles from './Input.module.css';
import InputProps from './Input.types';
const Input: React.FC<InputProps> = (props) => {
	const inputId = useId();
	const [attempted, setAttempted] = useState(false);
	const inputTagType =
		props.inputType === 'textarea' ? (
			<textarea
				id={inputId}
				className={
					props.required && attempted ? styles.required_input : styles.input
				}
				placeholder={props.placeHolder}
				onChange={props.onChange}
				value={props.value}
				onInput={() => setAttempted(true)}
			></textarea>
		) : (
			<input
				id={inputId}
				className={
					props.required && attempted ? styles.required_input : styles.input
				}
				placeholder={props.placeHolder}
				type={props.inputType}
				onChange={props.onChange}
				value={props.value}
				onInput={() => setAttempted(true)}
			/>
		);
	return (
		<div
			className={
				props.required && attempted
					? styles.input_container_required
					: styles.input_container
			}
			data-requiredtext={`${props.labelText} required`}
		>
			<label className={styles.label} htmlFor={inputId}>
				{props.labelText}
			</label>
			{inputTagType}
		</div>
	);
};

export { Input };
