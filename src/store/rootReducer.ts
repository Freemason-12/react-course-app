import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import { courseReducer } from './courses/reducer';
import { authorReducer } from './authors/reducer';
import { userReducer } from './user/reducer';
import { Course } from './courses/types';
import { Author } from './authors/types';
import { User } from './user/types';

export interface AppState {
	user: User;
	courses: Course[];
	authors: Author[];
}

export const initialState = {
	user: {
		isAuth: false,
		name: '',
		email: '',
		token: '',
		role: '',
	},
	courses: [],
	authors: [],
} as AppState;

const rootReducer = combineReducers({
	courses: courseReducer,
	authors: authorReducer,
	user: userReducer,
});

export const store = configureStore({
	reducer: rootReducer,
	preloadedState: initialState,
});

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = useDispatch.withTypes<AppDispatch>();
