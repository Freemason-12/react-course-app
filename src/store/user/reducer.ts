import { UserActions, User } from './types';
import { initialState } from '../rootReducer';

function userReducer(user: User = initialState.user, action) {
	switch (action.type) {
		case UserActions.LOGIN_USER: {
			return action.payload;
		}
		case UserActions.LOGOUT_USER: {
			return initialState.user as User;
		}
		default:
			return user;
	}
}

export { userReducer };
