import { apiAddress } from 'src/constants';
import { setUserAction, removeUserAction } from './actions';
import { initialState } from '../rootReducer';

export function fetchCurrentUser(authToken) {
	if (authToken === null || authToken === '')
		return async (dispatch) => dispatch(setUserAction(initialState.user));
	return async (dispatch) => {
		const response = await fetch(`${apiAddress}/users/me`, {
			headers: { Authorization: authToken },
		}).then((r) => r.json());
		dispatch(
			setUserAction({
				isAuth: true,
				name: response.result.name,
				email: response.result.email,
				token: authToken,
				role: response.result.role,
			})
		);
	};
}

export function removeCurrentUser(authToken) {
	if (authToken === null || authToken === '') return (dispatch) => dispatch();
	return async (dispatch) => {
		const response = await fetch(`${apiAddress}/logout`, {
			method: 'DELETE',
			headers: { Authorization: authToken },
		});
		dispatch(removeUserAction(response.toString()));
	};
}
