const enum UserActions {
	REGISTER_USER = 'REGISTER_USER',
	LOGIN_USER = 'AUTHENTICATE_USER',
	LOGOUT_USER = 'LOGOUT_USER',
}

interface User {
	isAuth: boolean;
	name: string;
	email: string;
	token: string;
	role: string;
}

export { UserActions, User };
