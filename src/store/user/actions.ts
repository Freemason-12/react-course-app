import { UserActions, User } from './types';

export const createUserAction = (payload: User) => ({
	type: UserActions.REGISTER_USER,
	payload,
});
export const setUserAction = (payload: User) => ({
	type: UserActions.LOGIN_USER,
	payload,
});
export const removeUserAction = (payload: string) => ({
	type: UserActions.LOGOUT_USER,
	payload,
});
