const enum CourseActions {
	ADD_COURSE = 'ADD_COURSE',
	REMOVE_COURSE = 'REMOVE_COURSE',
	SAVE_COURSE = 'SAVE_COURSE',
	SET_COURSES_LIST = 'SET_COURSES_LIST',
}

interface Course {
	id: string;
	title: string;
	description: string;
	creationDate: string;
	duration: number;
	authors: string[];
}

export { CourseActions, Course };
