import { apiAddress } from 'src/constants';
import {
	setCoursesListAction,
	addCourseAction,
	saveCourseAction,
	removeCourseAction,
} from './actions';
import { Course } from './types';

export async function fetchCourses(dispatch) {
	const response = await fetch(`${apiAddress}/courses/all`).then((r) =>
		r.json()
	);
	console.log(response);
	dispatch(setCoursesListAction(response.result));
}

export function addCourse(course: Course, authToken) {
	if (authToken === null || authToken === '') return (dispatch) => dispatch();
	return async (dispatch) => {
		const response = fetch(`${apiAddress}/courses/add`, {
			method: 'POST',
			headers: {
				Authorization: authToken,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(course),
		});
		console.log(JSON.parse(JSON.stringify(course)), response);
		dispatch(addCourseAction(course));
	};
}

export function saveCourse(course: Course, authToken) {
	if (authToken === null || authToken === '') return (dispatch) => dispatch();
	return async (dispatch) => {
		const response = await fetch(`${apiAddress}/courses/${course.id}`, {
			method: 'PUT',
			headers: {
				Authorization: authToken,
				'Content-Type': 'application/json',
				id: course.id,
			},
			body: JSON.stringify(course),
		});
		console.log(response);
		dispatch(saveCourseAction(course));
	};
}

export function removeCourse(courseId: string, authToken) {
	if (authToken === null || authToken === '') return (dispatch) => dispatch();
	return async (dispatch) => {
		const response = await fetch(`${apiAddress}/courses/${courseId}`, {
			method: 'DELETE',
			headers: {
				Authorization: authToken,
				id: courseId,
			},
		});
		console.log(response);
		dispatch(removeCourseAction(courseId));
	};
}
