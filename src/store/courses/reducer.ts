import { CourseActions, Course } from './types';
import { initialState } from '../rootReducer';

function courseReducer(
	state: Course[] = initialState.courses,
	action
): Course[] {
	switch (action.type) {
		case CourseActions.ADD_COURSE: {
			return [...state, action.payload];
		}
		case CourseActions.REMOVE_COURSE: {
			return state.filter((course) => course.id !== action.payload);
		}
		case CourseActions.SAVE_COURSE: {
			return [
				...state.filter((course) => course.id !== action.payload.id),
				action.payload,
			];
		}
		case CourseActions.SET_COURSES_LIST:
			return action.payload;
		default:
			return state;
	}
}

export { courseReducer };
