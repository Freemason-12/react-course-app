import { CourseActions, Course } from './types';

export const addCourseAction = (payload: Course) => ({
	type: CourseActions.ADD_COURSE,
	payload,
});
export const removeCourseAction = (payload: string) => ({
	type: CourseActions.REMOVE_COURSE,
	payload,
});
export const saveCourseAction = (payload: Course) => ({
	type: CourseActions.SAVE_COURSE,
	payload,
});
export const setCoursesListAction = (payload: Course[]) => ({
	type: CourseActions.SET_COURSES_LIST,
	payload,
});

// export { addCourseAction, removeCourseAction, saveCourseAction,  };
