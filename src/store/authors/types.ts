const enum AuthorActions {
	ADD_AUTHOR = 'ADD_AUTHOR',
	REMOVE_AUTHOR = 'REMOVE_AUTHOR',
	SAVE_AUTHOR = 'SAVE_AUTHOR',
	SET_AUTHORS_LIST = 'SET_AUTHORS_LIST',
}

interface Author {
	id: string;
	name: string;
}

export { AuthorActions, Author };
