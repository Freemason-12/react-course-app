import { apiAddress } from 'src/constants';
import { setAuthorsListAction, addAuthorAction } from './actions';
import { Author } from './types';

export async function fetchAuthors(dispatch) {
	const response = await fetch(`${apiAddress}/authors/all`).then((r) =>
		r.json()
	);
	dispatch(setAuthorsListAction(response.result));
}

export function addAuthor(author: Author, authToken) {
	if (authToken === null || authToken === '') return (dispatch) => dispatch();
	return async (dispatch) => {
		const response = await fetch(`${apiAddress}/authors/add`, {
			method: 'POST',
			headers: {
				Authorization: authToken,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(author),
		});
		console.log(response);
		dispatch(addAuthorAction(author));
	};
}
