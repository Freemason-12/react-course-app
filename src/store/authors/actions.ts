import { AuthorActions, Author } from './types';

export const addAuthorAction = (payload: Author) => ({
	type: AuthorActions.ADD_AUTHOR,
	payload,
});
export const removeAuthorAction = (payload: string) => ({
	type: AuthorActions.REMOVE_AUTHOR,
	payload,
});
export const saveAuthorAction = (payload: Author) => ({
	type: AuthorActions.SAVE_AUTHOR,
	payload,
});
export const setAuthorsListAction = (payload: Author[]) => ({
	type: AuthorActions.SET_AUTHORS_LIST,
	payload,
});
