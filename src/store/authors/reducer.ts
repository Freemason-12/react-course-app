import { AuthorActions, Author } from './types';
import { initialState } from '../rootReducer';

function authorReducer(
	state: Author[] = initialState.authors,
	action
): Author[] {
	switch (action.type) {
		case AuthorActions.ADD_AUTHOR: {
			return [...state, action.payload];
		}
		case AuthorActions.REMOVE_AUTHOR: {
			return state.filter((author) => author.id !== action.payload.id);
		}
		case AuthorActions.SAVE_AUTHOR: {
			return [
				state.filter((author) => author.id !== action.payload.id),
				action.payload,
			];
		}
		case AuthorActions.SET_AUTHORS_LIST:
			return action.payload;
		default:
			return state;
	}
}

export { authorReducer };
